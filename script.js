//Return the total value of 2 random dice roles
function rollDice() { 
    return Math.ceil(Math.random()*6) + Math.ceil(Math.random()*6);
}

//Initialize arrays
let possible = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

let count = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

//Roll dice 1000x and count results for each total value
//Remember that index of array starts at zero and thus 'count' array should include 13 zeroes
for (i=1; i<=1000; i++) {
    let result = rollDice();
    count[result] = count[result] + 1;
}

//Output
let destination = document.getElementById('chart-container')
let widthMultipler = 2

for (let j = 2; j < count.length; j++) {

    //Inline layout of graph
    let chartRowElement = document.createElement('div')
    chartRowElement.style.display = "flex"
    
    //Possible roll values as header
    let headerElement = document.createElement('div')
    headerElement.style.width = "20px"
    let headerText = document.createTextNode(possible[j-2])
    headerElement.appendChild(headerText)
    chartRowElement.appendChild(headerElement)
    
    //Style width and color of bar graph
    let barElement = document.createElement('div')
    barElement.style.backgroundColor = 'red'
    barElement.style.width = count[j]*widthMultipler+'px'
    let divText = document.createTextNode(count[j])
    barElement.appendChild(divText)
    chartRowElement.appendChild(barElement)

    destination.appendChild(chartRowElement)
}


